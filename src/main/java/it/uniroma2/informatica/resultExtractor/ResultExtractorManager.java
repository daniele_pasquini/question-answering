package it.uniroma2.informatica.resultExtractor;

import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.QueryResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Gestore per l'esecuzione della query inserita dall'utente.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class ResultExtractorManager {

    /**
     * Esecuzione della query
     * @param query query da eseguire
     * @return risultati della ricerca.
     */
    public List<QueryResult> executeQuery(Query query){
        return new ArrayList<QueryResult>();
    }
}