package it.uniroma2.informatica.oracolo;

import javax.xml.transform.Result;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller principale del sistema. L'Oracolo manger gestisce il flusso di esecuzione per rispondere alla domanda effettuata dall'utente.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class OracoloManager {

    /**
     * Cerca una risposta alla domanda dell'utente
     * @return lista di risultati
     */
    public List<Result> responseAnswer(){

        return new ArrayList<Result>();
    }
}
