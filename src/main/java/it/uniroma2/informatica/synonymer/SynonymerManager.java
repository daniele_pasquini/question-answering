package it.uniroma2.informatica.synonymer;

import it.uniroma2.informatica.domain.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Gestore per il recupero dei sinonimi a partire da una query. I sinonimi vengono resituiti sono sotto forma di query per una successiva ricereca all'interno dell'ontologia.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class SynonymerManager {

    /**
     * Data una query, trova i sinonimi per gli elementi contenuti all'interno della query
     * @param query query
     * @return lista di query contenente i sinonimi trovati.
     */
    public List<Query> fetchSynonyms(Query query){
        return new ArrayList<Query>();
    }
}
