package it.uniroma2.informatica.parser;

import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.Triple;
import it.uniroma2.informatica.domain.UserInput;


/**
 * Gestore per il parsing dell'input dell'utente. Il gestore si occupa di trasformare l'input dell'untente in una query.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class ParserManager {

    /**
     * Parsing dell'input
     * @param userInput input dell'utente
     * @return query
     */
    public Query parseInput(UserInput userInput){

        return new Query(new Triple(null,null,null));
    }
}
