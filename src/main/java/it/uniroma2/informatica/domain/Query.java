package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class Query {

    private Triple mTriple;

    public Query(Triple mTriple) {
        this.mTriple = mTriple;
    }

    public Triple getmTriple() {
        return mTriple;
    }

    public void setmTriple(Triple mTriple) {
        this.mTriple = mTriple;
    }
}
