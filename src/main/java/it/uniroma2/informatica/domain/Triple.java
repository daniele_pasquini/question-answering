package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class Triple {

    private Subject mSubject;
    private Predicate mPredicate;
    private Object  mObject;

    public Triple(Object mObject, Subject mSubject, Predicate mPredicate) {
        this.mObject = mObject;
        this.mSubject = mSubject;
        this.mPredicate = mPredicate;
    }


    public Object getmObject() {
        return mObject;
    }

    public void setmObject(Object mObject) {
        this.mObject = mObject;
    }

    public Subject getmSubject() {
        return mSubject;
    }

    public void setmSubject(Subject mSubject) {
        this.mSubject = mSubject;
    }

    public Predicate getmPredicate() {
        return mPredicate;
    }

    public void setmPredicate(Predicate mPredicate) {
        this.mPredicate = mPredicate;
    }
}
